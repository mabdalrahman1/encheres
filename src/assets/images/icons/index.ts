export { default as checkCircle } from './checkCircle.svg';
export { default as xCircle } from './xCircle.svg';
export { default as boy } from './boy.svg';
export { default as hand } from './hand.svg';
export { default as back } from './back.svg';
export { default as copy } from './copy.svg';
