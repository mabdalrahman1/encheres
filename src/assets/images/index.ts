export { default as emptyGallery } from './empty_gallery.svg';
export { default as fail } from './fail.svg';
export { default as generateModal } from './generateModal.svg';
export { default as icon } from './3Dpix.svg';
export { default as logo } from './logo.svg';
export { default as success } from './success.svg';
export * as icons from './icons';
