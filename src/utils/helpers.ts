export const sleep = (milliseconds: number) => new Promise(resolve => setTimeout(resolve, milliseconds));

export const retryOnFailure = async (operation: () => any, maxTries = 2, waitTimeMs = 10000) => {
	for (let triesLeft = maxTries; triesLeft > 0; triesLeft--) {
		try {
			return await operation();
		} catch (error) {
			if (triesLeft === 1) {
				throw error;
			}
			console.error(error);

			await sleep(waitTimeMs);
		}
	}
};
