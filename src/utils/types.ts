export type TApiCallState = 'initial' | 'calling' | 'done' | 'delay' | 'error';

export type TInput = {
	label: string;
	name: string;
	type: string;
	placeholder: string;
	value: string;
	handleValueChange: (event: any) => void;
	errorMessage: string;
};

export type TPixelad = {
	img: string;
	name: string;
};

export type TPixeladImage = {
	pixelad: string;
	head: string;
	shirt: string;
	pants: string;
	legs: string;
};

export type TSupply = {
	existingSupply: number;
	totalSupply: number;
};
