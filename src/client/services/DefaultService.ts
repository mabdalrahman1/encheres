/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { AccessToken } from '../models/AccessToken';
import type { CollectionTokenId } from '../models/CollectionTokenId';
import type { NFT } from '../models/NFT';
import type { Password } from '../models/Password';
import type { TokenSupply } from '../models/TokenSupply';
import type { TransactionInitialStatus } from '../models/TransactionInitialStatus';
import type { TransactionStatus } from '../models/TransactionStatus';
import type { UserId } from '../models/UserId';
import type { Username } from '../models/Username';
import type { VaultId } from '../models/VaultId';
import type { Wallet } from '../models/Wallet';
import type { WalletAddress } from '../models/WalletAddress';
import type { WalletId } from '../models/WalletId';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class DefaultService {

    /**
     * Register User
     * @param requestBody
     * @returns any Successful Operation
     * @throws ApiError
     */
    public static postApiV1Register(
        requestBody: {
            username?: Username;
            password?: Password;
        },
    ): CancelablePromise<{
        accessToken: AccessToken;
        userId: UserId;
        vaultId: VaultId;
        wallets: Array<Wallet>;
    }> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/v1/register',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                409: `User Already Exists`,
            },
        });
    }

    /**
     * Login User
     * @param requestBody
     * @returns any Successful Operation
     * @throws ApiError
     */
    public static postApiV1Login(
        requestBody: {
            username?: Username;
            password?: Password;
        },
    ): CancelablePromise<{
        accessToken: AccessToken;
        userId: UserId;
        vaultId: VaultId;
        wallets: Array<Wallet>;
    }> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/v1/login',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                403: `Forbidden - Password is incorrect`,
            },
        });
    }

    /**
     * Claim an NFT for a specific wallet
     * @param requestBody
     * @returns TransactionInitialStatus Successful Operation
     * @throws ApiError
     */
    public static postApiV1Claim(
        requestBody: {
            walletId: WalletId;
        },
    ): CancelablePromise<TransactionInitialStatus> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/v1/claim',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                500: `Internal Error`,
            },
        });
    }

    /**
     * Get transaction status.
     * @param txId
     * @returns TransactionStatus Successful Operation
     * @throws ApiError
     */
    public static getApiV1TxStatus(
        txId?: string,
    ): CancelablePromise<TransactionStatus> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/v1/tx_status',
            query: {
                'txId': txId,
            },
        });
    }

    /**
     * Get all tokens for user. Optionally filter by walletId.
     * @param walletId
     * @returns NFT Successful Operation
     * @throws ApiError
     */
    public static getApiV1GetTokens(
        walletId?: WalletId,
    ): CancelablePromise<Array<NFT>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/v1/get_tokens',
            query: {
                'walletId': walletId,
            },
        });
    }

    /**
     * Get the wallet address of a walletId.
     * @param walletId
     * @returns any Successful Operation
     * @throws ApiError
     */
    public static getApiV1WalletAddress(
        walletId?: WalletId,
    ): CancelablePromise<{
        walletAddress: WalletAddress;
    }> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/v1/wallet_address',
            query: {
                'walletId': walletId,
            },
        });
    }

    /**
     * Transfer Token
     * @param requestBody
     * @returns any Successful Operation
     * @throws ApiError
     */
    public static postApiV1Transfer(
        requestBody: {
            walletId: WalletId;
            collectionTokenId: CollectionTokenId;
            receiver: WalletAddress;
        },
    ): CancelablePromise<{
        status: string;
    }> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/v1/transfer',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Get token supply
     * @returns TokenSupply Successful Operation
     * @throws ApiError
     */
    public static getApiV1Supply(): CancelablePromise<TokenSupply> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/v1/supply',
        });
    }

}
