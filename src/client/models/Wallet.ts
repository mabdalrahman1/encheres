/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Asset } from './Asset';
import type { WalletAddress } from './WalletAddress';
import type { WalletId } from './WalletId';

/**
 * A wallet object containing walletId, walletAddress and asset.
 */
export type Wallet = {
    walletId: WalletId;
    walletAddress: WalletAddress;
    asset: Asset;
};

