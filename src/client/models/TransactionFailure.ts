/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Status of Fireblocks Transaction
 */
export type TransactionFailure = {
    msg: string;
};

