/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * The Collection(Contract) Token Id (External).
 */
export type CollectionTokenId = number;
