/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Existing & Total token supply.
 */
export type TokenSupply = {
    existingSupply: number;
    totalSupply: number;
};

