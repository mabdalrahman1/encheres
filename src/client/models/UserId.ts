/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Unique ID represnting the user
 */
export type UserId = number;
