/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * The address of the collection.
 */
export type CollectionId = string;
