/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Initial Status of Fireblocks Transaction
 */
export type TransactionInitialStatus = {
    txId: string;
    msg: string;
    status: string;
};

