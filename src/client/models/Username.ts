/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Username must be unique
 */
export type Username = string;
