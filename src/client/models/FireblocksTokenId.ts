/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Internal Token ID for the new token.
 */
export type FireblocksTokenId = number;
