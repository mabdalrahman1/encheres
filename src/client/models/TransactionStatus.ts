/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Status of Fireblocks Transaction
 */
export type TransactionStatus = {
    txId: string;
    status: string;
};

