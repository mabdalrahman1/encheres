/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CollectionId } from './CollectionId';
import type { CollectionTokenId } from './CollectionTokenId';
import type { FireblocksTokenId } from './FireblocksTokenId';
import type { MetadataURI } from './MetadataURI';
import type { TokenImage } from './TokenImage';
import type { TokenType } from './TokenType';
import type { WalletId } from './WalletId';

/**
 * An NFT object containing tokenId, walletId, collectionId, collectionTokenId, metadataURI.
 */
export type NFT = {
    FireblocksTokenId: FireblocksTokenId;
    walletId: WalletId;
    collectionId: CollectionId;
    collectionTokenId: CollectionTokenId;
    tokenType: TokenType;
    metadataURI: MetadataURI;
    tokenImage: TokenImage;
};

