import { FunctionComponent } from 'react';
import { useLogout } from '../hooks';

const Logout: FunctionComponent = () => {
	useLogout();
	return null;
};

export default Logout;
