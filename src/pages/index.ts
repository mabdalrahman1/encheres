export { default as Home } from './Home';
export { default as Gallery } from './Gallery';
export { default as Register } from './Register';
export { default as Login } from './Login';
export { default as Logout } from './Logout';
