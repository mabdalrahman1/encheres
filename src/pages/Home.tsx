import { FunctionComponent } from 'react';
import { Helmet } from 'react-helmet';

import { useHome } from '../hooks';
import { icons } from '../assets/images';
import { BidNFT } from '../components';
import { TPixelad } from '../utils/types';

export interface IHomeProps {
	pixelads: TPixelad[] | null;
	supply: number;
	setSupply: (supply: number) => void;
}

const Home: FunctionComponent<IHomeProps> = ({ pixelads, supply, setSupply }) => {
	const { shouldShowBidNFT, bidState, setBidState, onClickBidItem, handleCloseModal } = useHome(setSupply);

	return (
		<>
			<main className="page home">
				<Helmet>
					<title>3Dpix</title>
				</Helmet>
				<section className="hero">
					<h1 className="hero_heading">
						Start your <div>NFT auction experience</div>
					</h1>
					<div className="actions">
						<button onClick={onClickBidItem} className="actions-item">
							<img src={icons.boy} alt="" />
							<p>Bid an item for auction</p>
						</button>
						<button className="actions-item">
							<img src={icons.hand} alt="" />
							<p>Enter an auction URL</p>
						</button>
					</div>
				</section>

				<footer className="home_footer">
					<div className="footer_container">
						<div className="footer_numbers">
							{supply
								.toString()
								.split('')
								.map((digit, index) => (
									<div key={index} className="footer_number">
										<span>{digit}</span>
									</div>
								))}
						</div>
						<h2 className="footer_heading">
							NFT are auctioned <span>right now!</span>
						</h2>
					</div>
				</footer>

				{shouldShowBidNFT && (
					<BidNFT
						pixelads={pixelads}
						status={bidState}
						setGenerateStatus={setBidState}
						handleCloseModal={handleCloseModal}
					/>
				)}
			</main>
		</>
	);
};

export default Home;
