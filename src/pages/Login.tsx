import { FunctionComponent } from 'react';
import { Helmet } from 'react-helmet';
import { Modal } from 'flowbite-react';

import { Form } from '../components';
import { useLogin } from '../hooks';

const Login: FunctionComponent = () => {
	const {
		email,
		emailErrorMessage,
		password,
		passwordErrorMessage,
		shouldShowModal,
		doSubmit,
		handleChangeEmail,
		handleChangePassword,
	} = useLogin();

	return (
		<section className="page">
			<Helmet>
				<title>3Dpix Login</title>
			</Helmet>
			<Modal
				show={shouldShowModal}
				popup={true}
				size="lg"
				style={{ background: 'rgba(17, 24, 39, 0.9)', zIndex: 6000 }}
			>
				<div className="modal-form-shadow">
					<Modal.Body
						style={{
							padding: '0',
						}}
					>
						<Form
							title="Sign in to 3Dpix"
							inputs={[
								{
									label: 'Email address',
									name: 'email',
									type: 'email',
									placeholder: 'name@3Dpix.com',
									value: email,
									handleValueChange: handleChangeEmail,
									errorMessage: emailErrorMessage,
								},
								{
									label: 'Password',
									name: 'password',
									type: 'password',
									placeholder: 'Password',
									value: password,
									handleValueChange: handleChangePassword,
									errorMessage: passwordErrorMessage,
								},
							]}
							submit={{ value: 'Sign in', loadingValue: 'Signing in ...', handleSubmit: doSubmit }}
							afterSubmit={{
								value: 'Not registered?',
								link: {
									value: 'Create account',
									href: '/register',
								},
							}}
						/>
					</Modal.Body>
				</div>
			</Modal>
		</section>
	);
};

export default Login;
