import { FunctionComponent } from 'react';
import { Helmet } from 'react-helmet';
import { Modal } from 'flowbite-react';

import { Form } from '../components';
import { useRegister } from '../hooks';

const Register: FunctionComponent = () => {
	const {
		confirmPassword,
		confirmPasswordErrorMessage,
		email,
		emailErrorMessage,
		password,
		passwordErrorMessage,
		shouldShowModal,
		doSubmit,
		handleChangeConfirmPassword,
		handleChangeEmail,
		handleChangePassword,
	} = useRegister();

	return (
		<section className="page">
			<Helmet>
				<title>3Dpix Register</title>
			</Helmet>
			<Modal
				show={shouldShowModal}
				popup={true}
				size="lg"
				style={{ background: 'rgba(17, 24, 39, 0.9)', zIndex: 6000 }}
			>
				<div className="modal-form-shadow">
					<Modal.Body
						style={{
							padding: 0,
						}}
					>
						<Form
							title="Register to 3Dpix"
							inputs={[
								{
									label: 'Email address',
									name: 'email',
									type: 'email',
									placeholder: 'name@3Dpix.com',
									value: email,
									handleValueChange: handleChangeEmail,
									errorMessage: emailErrorMessage,
								},
								{
									label: 'Password',
									name: 'password',
									type: 'password',
									placeholder: 'Password',
									value: password,
									handleValueChange: handleChangePassword,
									errorMessage: passwordErrorMessage,
								},
								{
									label: 'Confirm password',
									name: 'confirm-password',
									type: 'password',
									placeholder: 'Confirm password',
									value: confirmPassword,
									handleValueChange: handleChangeConfirmPassword,
									errorMessage: confirmPasswordErrorMessage,
								},
							]}
							submit={{
								value: 'Create account',
								loadingValue: 'Creating a new account ...',
								handleSubmit: doSubmit,
							}}
							afterSubmit={{
								value: 'Registered?',
								link: {
									value: 'Login now',
									href: '/login',
								},
							}}
						/>
					</Modal.Body>
				</div>
			</Modal>
		</section>
	);
};

export default Register;
