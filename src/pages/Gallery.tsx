import { FunctionComponent } from 'react';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import { Button, Spinner } from 'flowbite-react';

import { emptyGallery, icons } from '../assets/images';
import { Card, SendCollectible } from '../components';
import { TPixelad } from '../utils/types';
import { useGallery } from '../hooks';

export interface IGalleryProps {
	pixelads: TPixelad[] | null;
	setPixelads: (pixelad: TPixelad[]) => void;
}

const Gallery: FunctionComponent<IGalleryProps> = props => {
	const { currentSelectingPixelad, galleryContent, pixelads, handleCloseModal, handleSendCollectible } =
		useGallery(props);

	return (
		<main className="page">
			<Helmet>
				<title>3Dpix Gallery</title>
			</Helmet>
			<section className="gallery">
				<div className="gallery_header">
					<h1 className="gallery_heading">Gallery</h1>
				</div>

				{
					{
						loading: (
							<div className="gallery_loading | gallery_no_content">
								<div className="gallery_spinner">
									<Spinner size="xl" />
								</div>
								<h2 className="gallery_loading__heading">Loading your Pixelads...</h2>
							</div>
						),

						pixelads: pixelads && (
							<div className="gallery_content">
								{pixelads.map(pixelad => (
									<Card key={pixelad.name} name={pixelad.name} img={pixelad.img} />
								))}
							</div>
						),

						empty: (
							<div className="gallery_empty | gallery_no_content text-white">
								<img className="gallery_empty__img" src={emptyGallery} alt="" />
								<h2 className="gallery_empty__heading">No pixelads yet</h2>
								<Link to="/">
									<Button
										style={{
											borderRadius: 0,
											height: '50px',
											marginBottom: '20px',
											width: '340px',
										}}
									>
										<span>Generate now</span>
									</Button>
								</Link>
								<div className="flex gap-1">
									<p>Already generated one?</p>
									<a href="javascript:location.reload();">
										<p className="gallery_empty__refresh">Refresh</p>
									</a>
								</div>
							</div>
						),
					}[galleryContent]
				}
			</section>

			{currentSelectingPixelad && (
				<SendCollectible pixelad={currentSelectingPixelad} handleCloseModal={handleCloseModal} />
			)}
		</main>
	);
};

export default Gallery;
