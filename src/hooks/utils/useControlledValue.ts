import { useState } from 'react';

const useControlledValue = <T>(initialValue: T): [T, (event: React.ChangeEvent<HTMLInputElement>) => void] => {
	const [value, setValue] = useState(initialValue);

	const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setValue(event.target.value as any);
	};

	return [value, handleChange];
};

export default useControlledValue;
