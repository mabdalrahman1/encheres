import { useCookies } from 'react-cookie';
import { OpenAPI, DefaultService as apiService } from '../../client';

OpenAPI.BASE = import.meta.env.VITE_API_BASE_URL;

export type TUser = { JWT: string; userId: number; vaultId: number; walletId: number; walletAddress: string };

const useAuth = () => {
	const [cookies, setCookie, removeCookie] = useCookies(['currentUser']);
	const { currentUser } = cookies;
	const isAuth = !!currentUser;

	const login = async (email: string, password: string) => {
		try {
			const response = await apiService.postApiV1Login({ username: email, password });
			const user = response;
			setCookie('currentUser', {
				JWT: user.accessToken,
				userId: user.userId,
				vaultId: user.vaultId,
				walletId: user.wallets[0].walletId,
				walletAddress: user.wallets[0].walletAddress,
			});
		} catch (error) {}
	};

	const register = async (email: string, password: string) => {
		try {
			const response = await apiService.postApiV1Register({ username: email, password });
			const user = response;
			setCookie('currentUser', {
				JWT: user.accessToken,
				userId: user.userId,
				vaultId: user.vaultId,
				walletId: user.wallets[0].walletId,
				walletAddress: user.wallets[0].walletAddress,
			});
		} catch (error) {}
	};

	const logout = async () => {
		removeCookie('currentUser');
	};

	return {
		currentUser,
		isAuth,
		login,
		logout,
		register,
	};
};

export default useAuth;
