import { OpenAPI, DefaultService as apiService } from '../../client';
import { sleep } from '../../utils/helpers';
import { TUser } from './useAuth';

OpenAPI.BASE = import.meta.env.VITE_API_BASE_URL;

const useClient = (user: TUser) => {
	OpenAPI.HEADERS = { Authorization: `Bearer ${user.JWT}` };

	const getSupply = () => {
		return apiService.getApiV1Supply();
	};

	const claim = () => {
		return apiService.postApiV1Claim({ walletId: user.walletId });
	};

	const getTxStatus = (txId: string) => {
		return apiService.getApiV1TxStatus(txId);
	};

	const getTokens = () => {
		return apiService.getApiV1GetTokens(user.walletId);
	};

	// TODO: Fix the type when swagger file is updated.
	const transfer = async (collectionTokenId: number, receiver: string) => {
		const response: any = await apiService.postApiV1Transfer({
			walletId: user.walletId,
			collectionTokenId,
			receiver,
		});
		return response as Promise<{
			msg: string;
			status: any;
			txId: string;
		}>;
	};

	const pollTxStatus = async (
		transactionId: string,
		maxTimeWaiting = 120000,
		intervalTime = 10000,
	): Promise<string> => {
		const _checkTxStatus = async (transactionId: string) => {
			const response = await getTxStatus(transactionId);
			return response.status;
		};
		for (let waitedTime = 0; waitedTime < maxTimeWaiting; waitedTime += intervalTime) {
			const txStatus = await _checkTxStatus(transactionId);

			if (['COMPLETED', 'FAILED', 'REJECTED'].includes(txStatus)) {
				return txStatus;
			}

			await sleep(intervalTime);
		}

		return 'DELAY';
	};

	return {
		claim,
		getSupply,
		getTokens,
		getTxStatus,
		pollTxStatus,
		transfer,
	};
};

export default useClient;
