import { useEffect, useState } from 'react';
import { useAuth, useControlledValue } from '../';

const useLogin = () => {
	const { isAuth, login } = useAuth();

	if (isAuth) {
		window.location.href = '/';
	}

	const [email, handleChangeEmail] = useControlledValue('');
	const [password, handleChangePassword] = useControlledValue('');
	const [emailErrorMessage, setEmailErrorMessage] = useState('');
	const [passwordErrorMessage, setPasswordErrorMessage] = useState('');
	const [shouldShowModal, setShouldShowModal] = useState(false);

	const handleError = (message: string) => {
		if (message === '') {
			setEmailErrorMessage('');
			setPasswordErrorMessage('');
			return;
		}

		switch (message) {
			case 'Not Found':
				setEmailErrorMessage('User was not found');
				break;
			case 'Forbidden - Password is incorrect':
				setPasswordErrorMessage('Incorrect Password');
				break;
			default:
				setEmailErrorMessage('User was not found');
				break;
		}
	};

	const doSubmit = async (ev: any) => {
		ev.preventDefault();

		try {
			const response = await login(email, password);
			window.location.href = '/';
			return response;
		} catch (ex: any) {
			handleError(ex.message);
			return ex;
		}
	};

	useEffect(() => setShouldShowModal(true), []);

	useEffect(() => {
		handleError('');
	}, [email, password]);

	return {
		email,
		emailErrorMessage,
		password,
		passwordErrorMessage,
		shouldShowModal,
		doSubmit,
		handleChangeEmail,
		handleChangePassword,
	};
};

export default useLogin;
