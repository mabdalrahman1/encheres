import { useEffect } from 'react';
import { useAuth } from '../';

const useLogout = () => {
	const { logout } = useAuth();
	useEffect(() => {
		logout();
		window.location.href = '/#/login';
	}, []);
};

export default useLogout;
