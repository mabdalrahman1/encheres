import { useEffect, useState } from 'react';
import { IGalleryProps } from '../../pages/Gallery';
import { TPixelad } from '../../utils/types';
import { useAuth, useClient } from '../';

type TGalleryContent = 'loading' | 'empty' | 'pixelads';

const useGallery = ({ pixelads, setPixelads }: IGalleryProps) => {
	const { isAuth, currentUser } = useAuth();

	if (!isAuth) {
		window.location.href = '/#/login';
	}

	const client = useClient(currentUser);
	const [currentSelectingPixelad, setCurrentSelectingPixelad] = useState<TPixelad | null>(null);
	const [galleryContent, setGalleryContent] = useState<TGalleryContent>('loading');

	const handleSendCollectible = (pixelad: TPixelad) => {
		setCurrentSelectingPixelad(pixelad);
	};

	const handleCloseModal = () => {
		setCurrentSelectingPixelad(null);
	};

	useEffect(() => {
		const fetchData = async () => {
			try {
				const response: any[] = await client.getTokens();
				const _pixelads = response.map(item => ({
					img: item.tokenImage + '',
					name: `Pixelad #${item.collectionTokenId}`,
				}));

				setPixelads(_pixelads);
			} catch (ex) {
				console.error(ex);
			}
		};
		fetchData();
	}, []);

	useEffect(() => {
		if (!pixelads) {
			setGalleryContent('loading');
			return;
		}
		if (pixelads.length === 0) {
			setGalleryContent('empty');
			return;
		}
		setGalleryContent('pixelads');
	}, [pixelads]);

	return {
		currentSelectingPixelad,
		galleryContent,
		pixelads,
		handleCloseModal,
		handleSendCollectible,
	};
};

export default useGallery;
