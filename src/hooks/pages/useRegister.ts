import { useEffect, useState } from 'react';
import { useAuth, useControlledValue } from '../';

const useRegister = () => {
	const { isAuth, register } = useAuth();

	if (isAuth) {
		window.location.href = '/';
	}

	const [email, handleChangeEmail] = useControlledValue('');
	const [password, handleChangePassword] = useControlledValue('');
	const [confirmPassword, handleChangeConfirmPassword] = useControlledValue('');
	const [emailErrorMessage, setEmailErrorMessage] = useState('');
	const [passwordErrorMessage, setPasswordErrorMessage] = useState('');
	const [confirmPasswordErrorMessage, setConfirmPasswordErrorMessage] = useState('');
	const [shouldShowModal, setShouldShowModal] = useState(false);

	const handleError = (status: number) => {
		if (status === 0) {
			setEmailErrorMessage('');
			setPasswordErrorMessage('');
			setConfirmPasswordErrorMessage('');
		} else {
			switch (status) {
				case 409:
					setEmailErrorMessage('This User Name Is Already Taken');
					break;
				default:
					setEmailErrorMessage('');
					break;
			}
		}
	};

	const doSubmit = async (ev: any) => {
		ev.preventDefault();

		if (password !== confirmPassword) {
			setPasswordErrorMessage('Passwords does not match.');
			setConfirmPasswordErrorMessage('Passwords does not match.');
			return new Error('Passwords does not match.');
		}

		try {
			const response = await register(email, password);
			window.location.href = '/';
			return response;
		} catch (ex: any) {
			handleError(ex.status);
			return ex;
		}
	};

	useEffect(() => setShouldShowModal(true), []);

	useEffect(() => {
		handleError(0);
	}, [email, password, confirmPassword]);

	return {
		confirmPassword,
		confirmPasswordErrorMessage,
		email,
		emailErrorMessage,
		password,
		passwordErrorMessage,
		shouldShowModal,
		doSubmit,
		handleChangeConfirmPassword,
		handleChangeEmail,
		handleChangePassword,
	};
};

export default useRegister;
