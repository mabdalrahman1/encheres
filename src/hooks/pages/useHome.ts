import { useEffect, useState } from 'react';
import { TApiCallState } from '../../utils/types';

const useHome = (setSupply: (supply: number) => void) => {
	const [shouldShowBidNFT, setShouldShowBidNFT] = useState(false);
	const [bidState, setBidState] = useState<TApiCallState>('initial');

	const handleCloseModal = () => {
		setShouldShowBidNFT(false);
	};

	const onClickBidItem = () => {
		setBidState(bidState === 'done' || bidState === 'error' ? 'initial' : bidState);
		setShouldShowBidNFT(true);
	};

	useEffect(() => {
		const fetchData = async () => {
			try {
				setSupply(3425);
			} catch (error) {
				console.error(error);
			}
		};
		fetchData();
	}, []);

	return {
		bidState,
		shouldShowBidNFT,
		handleCloseModal,
		onClickBidItem,
		setBidState,
	};
};

export default useHome;
