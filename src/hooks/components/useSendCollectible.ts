import { useEffect, useState } from 'react';

import { TApiCallState, TPixelad } from '../../utils/types';
import { useClient, useControlledValue } from '../';
import { retryOnFailure } from '../../utils/helpers';
import { useAuth } from '../';

const useSendCollectible = (pixelad: TPixelad) => {
	const [receiver, handleChangeReceiver] = useControlledValue('');
	const [collectibleStatus, setCollectibleStatus] = useState<TApiCallState>('initial');
	const [shouldDisabledSubmit, setShouldDisabledSubmit] = useState(true);
	const [helperMessage, setHelperMessage] = useState({ text: '', color: 'text-red-500' });
	const [submitValue, setSubmitValue] = useState('Send collectible');
	const { currentUser } = useAuth();
	const client = useClient(currentUser);

	const doTransfer = async () => {
		const pixeladId = Number(pixelad.name.slice(9));
		const transferResponse = await client.transfer(pixeladId, receiver);
		const transactionId = transferResponse.txId;
		const txStatus = await client.pollTxStatus(transactionId);

		if (txStatus === 'FAILED' || txStatus === 'REJECTED') {
			throw new Error('Transaction Failed');
		}

		if (txStatus === 'DELAY') {
			setCollectibleStatus('delay');
			return;
		}

		setCollectibleStatus('done');
	};

	const handleSubmit = async () => {
		setCollectibleStatus('calling');
		try {
			await retryOnFailure(doTransfer);
		} catch (error: any) {
			if (error.status === 422) {
				setHelperMessage({ text: 'Wrong wallet address!', color: 'text-red-500' });
				setCollectibleStatus('initial');
				return;
			}

			console.error(error);
			setCollectibleStatus('error');
		}
	};

	const handleTryAgain = () => setCollectibleStatus('initial');

	useEffect(() => {
		setSubmitValue(collectibleStatus === 'initial' ? 'Send collectible' : 'Sending...');
	}, [collectibleStatus]);

	useEffect(() => {
		setShouldDisabledSubmit(collectibleStatus === 'calling' || receiver === '');
	}, [receiver, collectibleStatus]);

	return {
		collectibleStatus,
		helperMessage,
		receiver,
		shouldDisabledSubmit,
		submitValue,
		handleChangeReceiver,
		handleSubmit,
		handleTryAgain,
	};
};

export default useSendCollectible;
