import { useState } from 'react';

const useCard = () => {
	const [shouldShowButton, setShouldShowButton] = useState(false);

	const handleHoverCard = (isMouseEnter: boolean) => {
		return () => setShouldShowButton(isMouseEnter);
	};

	return { shouldShowButton, handleHoverCard };
};

export default useCard;
