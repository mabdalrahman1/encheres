import { useState } from 'react';

import { useAuth, useClient, useControlledValue } from '..';
import { TPixelad } from '../../utils/types';
import { retryOnFailure } from '../../utils/helpers';
import { IGeneratePixeladProps } from '../../components/BidNFT';
import { NFT } from '../../client';

const useBidNFT = ({ pixelads, status, setGenerateStatus, handleCloseModal }: IGeneratePixeladProps) => {
	const { currentUser } = useAuth();
	const client = useClient(currentUser);
	const [auctionTime, handleChangeAuctionTime] = useControlledValue('');
	const [initialValue, handleChangeInitialValue] = useControlledValue('');

	const doClaim = async () => {
		const claimResponse = await client.claim();
		const transactionId = claimResponse.txId;
		const txStatus = await client.pollTxStatus(transactionId);

		if (txStatus === 'FAILED' || txStatus === 'REJECTED') {
			throw new Error('Claim failed');
		}

		if (txStatus === 'DELAY') {
			setGenerateStatus('delay');
			return;
		}

		const tokens: NFT[] = await client.getTokens();
		const newToken = tokens[tokens.length - 1];

		if (!newToken) {
			setGenerateStatus('done');
			return;
		}

		setGenerateStatus('done');
	};

	const handleStartAuction = async () => {
		setGenerateStatus('calling');

		try {
			await retryOnFailure(doClaim);
		} catch (error: any) {
			setGenerateStatus('error');
			console.error(error);
		}
	};

	const onCloseModal = () => {
		setGenerateStatus(status !== 'calling' ? 'initial' : status);
		handleCloseModal();
	};

	return {
		auctionTime,
		initialValue,
		pixelads,
		status,
		handleCloseModal,
		handleStartAuction,
		onCloseModal,
		handleChangeAuctionTime,
		setGenerateStatus,
		handleChangeInitialValue,
	};
};

export default useBidNFT;
