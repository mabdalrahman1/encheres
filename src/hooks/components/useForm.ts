import { useState } from 'react';
import { TInput } from '../../utils/types';

const useForm = (inputs: TInput[], handleSubmit: (event: any) => Promise<any>) => {
	const isDisabledSubmit = inputs.some(input => input.value === '');
	const [isSubmitting, setIsSubmitting] = useState(false);

	const onFormSubmit = async (event: any) => {
		setIsSubmitting(true);
		const response = await handleSubmit(event);

		if (response instanceof Error) {
			setIsSubmitting(false);
		}
	};

	return {
		isDisabledSubmit,
		isSubmitting,
		onFormSubmit,
	};
};

export default useForm;
