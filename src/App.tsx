import { useState } from 'react';
import { HashRouter as Router, Route, Routes } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import { icon } from './assets/images';
import { Layout } from './components';
import { Home, Gallery, Register, Login, Logout } from './pages';
import { TPixelad } from './utils/types';

function App() {
	const [supply, setSupply] = useState(0);
	const [pixelads, setPixelads] = useState<TPixelad[] | null>([
		{
			name: 'Pixelad #1',
			img: 'https://static.fireblocks.io/nft/415624429301ae45d8546f68e08e46fd467f39d9/media/aHR0cHM6Ly9pcGZzLmlvL2lwZnMvUW1iR29HRkpCaEU0cU44cjdZU3FGUGhrSm43TlZDNFlwb0tWNE5BQWdDTHYxWi8yODkucG5n',
		},
		{
			name: 'Pixelad #2',
			img: 'https://static.fireblocks.io/nft/415624429301ae45d8546f68e08e46fd467f39d9/media/aHR0cHM6Ly9pcGZzLmlvL2lwZnMvUW1iR29HRkpCaEU0cU44cjdZU3FGUGhrSm43TlZDNFlwb0tWNE5BQWdDTHYxWi8yODkucG5n',
		},
	]);

	if (import.meta.env.PROD) {
		console.log = () => {};
	}

	return (
		<Router>
			<Helmet>
				<link rel="icon" type="image/svg+xml" href={icon} />
			</Helmet>
			<Layout>
				<Routes>
					<Route path="/" element={<Home supply={supply} setSupply={setSupply} pixelads={pixelads} />} />
					<Route path="/gallery" element={<Gallery pixelads={pixelads} setPixelads={setPixelads} />} />
					<Route path="/register" element={<Register />} />
					<Route path="/login" element={<Login />} />
					<Route path="/logout" element={<Logout />} />
				</Routes>
			</Layout>
		</Router>
	);
}

export default App;
