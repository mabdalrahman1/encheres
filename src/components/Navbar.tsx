import { FunctionComponent } from 'react';
import { Button, Navbar as FlowBiteNavbar } from 'flowbite-react';
import { NavbarCollapse } from 'flowbite-react/lib/esm/components/Navbar/NavbarCollapse';
import { NavbarLink } from 'flowbite-react/lib/esm/components/Navbar/NavbarLink';

import { logo } from '../assets/images';
import { useAuth } from '../hooks';

const Navbar: FunctionComponent = () => {
	const { isAuth } = useAuth();

	return (
		<header style={{ position: 'fixed', top: 0, width: '100vw', zIndex: '1000' }}>
			<FlowBiteNavbar
				fluid={true}
				style={{
					backgroundColor: '#1F2A37',
					padding: '22px 24px',
				}}
			>
				{isAuth && (
					<>
						<div />

						<div className="grid gap-4 order-2 grid-flow-col place-items-center">
							<a href="/#/logout">
								<p className="font-normal text-blue-500">Sign Out</p>
							</a>
							<Button>Connect wallet</Button>
						</div>
					</>
				)}

				<div className="nav_brand_wrapper">
					<FlowBiteNavbar.Brand href="/">
						<img src={logo} className="mr-3 h-6 sm:h-9" alt="3Dpix Logo" style={{ zIndex: 5000 }} />
					</FlowBiteNavbar.Brand>
				</div>
			</FlowBiteNavbar>
		</header>
	);
};

export default Navbar;
