import { FunctionComponent } from 'react';
import { Link } from 'react-router-dom';
import { Button, Toast } from 'flowbite-react';
import { icons } from '../../assets/images';

interface ISuccessToastProps {
	title: string;
	message: string;
	button: { text: string; to: string };
}

const SuccessToast: FunctionComponent<ISuccessToastProps> = ({ title, message, button }) => (
	<div className="toast">
		<Toast
			style={{
				boxShadow: '0 1px 2px rgba(0, 0, 0, 0.08)',
				borderRadius: '6px',
			}}
		>
			<div className="flex items-start">
				<div className="text-sm text-gray-700">
					<div className="flex gap-2 mb-2">
						<img src={icons.checkCircle} alt="" />
						<div className="font-semibold">{title}</div>
						<Toast.Toggle className="text-gray-700 rounded-none" />
					</div>

					<div className="mb-3">{message}</div>

					<Link to={button.to}>
						<Button size="xs">{button.text}</Button>
					</Link>
				</div>
			</div>
		</Toast>
	</div>
);

export default SuccessToast;
