import { FunctionComponent } from 'react';
import { Link } from 'react-router-dom';
import { Button, Label, Spinner, TextInput } from 'flowbite-react';
import { useForm } from '../hooks';
import { TInput } from '../utils/types';

interface IFormProps {
	title: string;
	inputs: TInput[];
	submit: {
		value: string;
		loadingValue: string;
		handleSubmit: (event: any) => Promise<any>;
	};
	afterSubmit: {
		value: string;
		link: {
			value: string;
			href: string;
		};
	};
}

const Form: FunctionComponent<IFormProps> = ({ title, inputs, submit, afterSubmit }) => {
	const { isDisabledSubmit, isSubmitting, onFormSubmit } = useForm(inputs, submit.handleSubmit);

	return (
		<form className="bg-gray-700" onSubmit={onFormSubmit}>
			<div className="space-y-6 px-6 pb-4 sm:pb-6 lg:px-8 xl:pb-8">
				<h1 className="pt-8 text-white text-xl font-semibold">{title}</h1>

				{inputs.map(input => (
					<div key={input.name}>
						<div className="mb-2 block">
							<Label htmlFor="password" value={input.label} style={{ color: '#fff' }} />
						</div>
						<TextInput
							id={input.name}
							type={input.type}
							placeholder={input.placeholder}
							required={true}
							value={input.value}
							disabled={isSubmitting}
							onChange={input.handleValueChange}
							helperText={
								input.errorMessage && (
									<span style={{ color: 'red' }} className="font-normal">
										{input.errorMessage}
									</span>
								)
							}
							style={{
								backgroundColor: '#4B5563',
								border: '1px solid #6B7280',
								borderRadius: 0,
								color: '#fff',
							}}
						/>
					</div>
				))}

				{isSubmitting ? (
					<Button
						type="submit"
						disabled={true}
						style={{
							backgroundColor: 'rgb(55 65 81)',
							borderColor: 'white',
							borderRadius: 0,
							paddingBlock: '2.5px',
							width: '100%',
						}}
						color="dark"
					>
						<Spinner size={'sm'} className="mr-3" />
						<p>{submit.loadingValue}</p>
					</Button>
				) : (
					<Button
						type="submit"
						disabled={isDisabledSubmit}
						style={{
							width: '100%',
							borderRadius: 0,
							paddingBlock: '2.5px',
						}}
					>
						{submit.value}
					</Button>
				)}

				<div className="flex gap-2 text-sm font-medium text-gray-400 dark:text-gray-300">
					{afterSubmit.value}
					<Link
						className="text-sm text-blue-700 hover:underline dark:text-blue-500"
						to={afterSubmit.link.href}
					>
						{afterSubmit.link.value}
					</Link>
				</div>
			</div>
		</form>
	);
};

export default Form;
