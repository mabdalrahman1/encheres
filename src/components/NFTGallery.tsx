import { FunctionComponent } from 'react';
import { TPixelad } from '../utils/types';
import Card from './Card';

interface INFTGalleryProps {
	pixelads: TPixelad[] | null;
	pixelad: TPixelad | null;
	setPixelad: (pixelad: TPixelad) => void;
}

const NFTGallery: FunctionComponent<INFTGalleryProps> = ({ pixelad, pixelads, setPixelad }) => {
	if (!pixelads) {
		return null;
	}

	const selectedStyle = {
		border: '2px solid #1C64F2',
		borderRadius: '8px',
	};

	return (
		<div className="gallery_container">
			{pixelads.map(p => (
				<div
					onClick={() => setPixelad(p)}
					key={p.name}
					style={p.name === pixelad?.name ? selectedStyle : {}}
					className="cursor-pointer"
				>
					<Card img={p.img} name={p.name} />
				</div>
			))}
		</div>
	);
};

export default NFTGallery;
