import { FunctionComponent } from 'react';

import { Navbar } from './';

interface ILayoutProps {
	children: JSX.Element;
}

const Layout: FunctionComponent<ILayoutProps> = ({ children }) => {
	return (
		<>
			<Navbar />
			{children}
		</>
	);
};

export default Layout;
