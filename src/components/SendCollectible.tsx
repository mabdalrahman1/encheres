import { FunctionComponent } from 'react';
import { Button, Label, Modal, TextInput } from 'flowbite-react';

import { fail, success } from '../assets/images';
import { TPixelad } from '../utils/types';
import Card from './Card';
import { useSendCollectible } from '../hooks';

export interface ISendCollectibleProps {
	pixelad: TPixelad;
	handleCloseModal: () => void;
}

const SendCollectible: FunctionComponent<ISendCollectibleProps> = ({ pixelad, handleCloseModal }) => {
	const {
		collectibleStatus,
		helperMessage,
		receiver,
		shouldDisabledSubmit,
		submitValue,
		handleChangeReceiver,
		handleSubmit,
		handleTryAgain,
	} = useSendCollectible(pixelad);

	const onKeyDown = (event: React.KeyboardEvent) => {
		if (event.code === 'Escape') {
			handleCloseModal();
		}
	};

	return (
		<Modal
			show={true}
			popup={true}
			onClose={handleCloseModal}
			onKeyDown={event => onKeyDown(event)}
			tabIndex={0}
			style={{ background: 'rgba(17, 24, 39, 0.9)', zIndex: 6000 }}
		>
			<div className="send-collectible_heading-wrapper">
				<Modal.Header
					style={{
						borderRadius: 0,
						border: '1px solid #4B5563',
						padding: '24px',
					}}
					className="bg-gray-700"
				>
					<span className="send-collectible_heading">Send a collectible</span>
				</Modal.Header>
			</div>

			<Modal.Body
				style={{
					borderRadius: 0,
					border: '1px solid #4B5563',
				}}
				className="bg-gray-700"
			>
				{(collectibleStatus === 'initial' || collectibleStatus === 'calling') && (
					<>
						<div className="card_wrapper">
							<Card name={pixelad.name} img={pixelad.img} />
						</div>
						<div className="send">
							<div>
								<div className="mb-2 block">
									<Label htmlFor="receiver" value="Send to" style={{ color: '#fff' }} />
								</div>
								<TextInput
									id="receiver"
									type="text"
									placeholder="Enter a receiver wallet address"
									required={true}
									value={receiver}
									disabled={collectibleStatus === 'calling'}
									onChange={handleChangeReceiver}
									helperText={
										helperMessage.text && (
											<span className={`font-normal ${helperMessage.color}`}>
												{helperMessage.text}
											</span>
										)
									}
									style={{
										backgroundColor: '#374151',
										border: '1px solid #4B5563',
										borderRadius: 0,
										color: '#9CA3AF',
									}}
								/>
							</div>
						</div>
					</>
				)}

				{collectibleStatus === 'error' && (
					<div className="send-collectible__error">
						<img className="ml-4" src={fail} alt="Fail image." />
						<div>
							<p className="text-center text-2xl mb-2">There was a problem sending</p>
							<h2 className="text-center text-2xl font-bold">{pixelad.name}</h2>
						</div>
						<Button
							style={{
								borderRadius: 0,
							}}
							onClick={handleTryAgain}
						>
							Try again
						</Button>
					</div>
				)}

				{collectibleStatus === 'delay' && (
					<div>
						<h2 className="home-modal_heading">It seems like it's taking too long...</h2>
						<p className="home-modal_content">The Pixelad is transfer will be finished shortly</p>
					</div>
				)}

				{collectibleStatus === 'done' && (
					<div className="sent-collectable_wrapper text-2xl">
						<img src={success} alt="Send a collectible successes." className="sent-collectable_img" />
						<div className="text">
							<p className="font-bold">{pixelad.name}</p>
							<p>
								was sent to <span className="font-bold">{receiver}</span>
							</p>
						</div>
					</div>
				)}
			</Modal.Body>

			{['initial', 'sending'].includes(collectibleStatus) && (
				<Modal.Footer
					style={{
						borderRadius: 0,
						border: '1px solid #4B5563',
						display: 'flex',
						justifyContent: 'space-between',
					}}
					className="bg-gray-700"
				>
					<div className="flex items-center gap-2">
						<button
							className="cancel"
							onClick={handleCloseModal}
							disabled={collectibleStatus === 'calling'}
						>
							Cancel
						</button>
					</div>
					<Button
						style={{
							borderRadius: 0,
						}}
						disabled={shouldDisabledSubmit}
						type="submit"
						onClick={handleSubmit}
					>
						{submitValue}
					</Button>
				</Modal.Footer>
			)}
		</Modal>
	);
};

export default SendCollectible;
