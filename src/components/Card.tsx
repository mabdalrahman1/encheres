import { FunctionComponent } from 'react';
import { useCard } from '../hooks';

interface ICardProps {
	img: string;
	name: string;
}

const Card: FunctionComponent<ICardProps> = ({ img, name }) => {
	const { shouldShowButton, handleHoverCard } = useCard();

	return (
		<article className="card">
			<header className="card_header">
				<img className="card_image" src={img} alt="Pixelad picture." />
			</header>
			<div className="card_content">
				<h2 className="card_heading">{name}</h2>
			</div>
		</article>
	);
};

export default Card;
