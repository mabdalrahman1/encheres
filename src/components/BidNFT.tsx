import { FunctionComponent, useState } from 'react';
import { Link } from 'react-router-dom';
import { Button, Modal, Spinner } from 'flowbite-react';
import { ModalBody } from 'flowbite-react/lib/esm/components/Modal/ModalBody';

import { fail, icons } from '../assets/images';
import Card from './Card';
import { TPixelad } from '../utils/types';
import { useBidNFT } from '../hooks';
import NFTGallery from './NFTGallery';

type TApiCallState = 'initial' | 'settingData' | 'calling' | 'done' | 'delay' | 'error';

export interface IGeneratePixeladProps {
	pixelads: TPixelad[] | null;
	status: TApiCallState;
	setGenerateStatus: (status: TApiCallState) => void;
	handleCloseModal: () => void;
}

const BidNFT: FunctionComponent<IGeneratePixeladProps> = props => {
	const {
		auctionTime,
		initialValue,
		pixelads,
		status,
		handleCloseModal,
		handleStartAuction,
		onCloseModal,
		handleChangeAuctionTime,
		setGenerateStatus,
		handleChangeInitialValue,
	} = useBidNFT(props);
	const [pixelad, setPixelad] = useState<TPixelad | null>(null);

	const onKeyDown = (event: React.KeyboardEvent) => {
		if (event.code === 'Escape') {
			handleCloseModal();
		}
	};

	return (
		<Modal
			className="generateModal"
			style={{ background: 'rgba(17, 24, 39, 0.9)', zIndex: 6000 }}
			tabIndex={0}
			show={true}
			popup={true}
			size={'lg'}
			onClose={onCloseModal}
			onKeyDown={event => onKeyDown(event)}
		>
			<Modal.Header style={{ height: '75px', display: 'flex', alignItems: 'center' }} className="bg-gray-700">
				<span
					style={{
						color: '#fff',
						padding: '24px',
					}}
				>
					Bid NFT for auction
				</span>
			</Modal.Header>

			<ModalBody
				className="bg-gray-700"
				style={{
					padding: '2rem 2rem',
					borderTop: '1px solid #4B5563',
					borderBottom: '1px solid #4B5563',
				}}
			>
				{
					{
						initial: pixelads && (
							<div>
								<h3 style={{ marginBottom: '8px' }}>Select NFT</h3>
								<div
									style={{
										display: 'grid',
										justifyContent: 'center',
										justifyItems: 'center',
									}}
								>
									<NFTGallery pixelad={pixelad} setPixelad={setPixelad} pixelads={pixelads} />
								</div>
							</div>
						),

						settingData: (
							<div
								style={{
									display: 'grid',
									gap: '2rem',
								}}
							>
								<div style={{ gap: '1rem', display: 'flex' }}>
									<img
										className="cursor-pointer"
										src={icons.back}
										alt=""
										onClick={() => setGenerateStatus('initial')}
									/>
									<p
										style={{
											fontSize: '20px',
											fontWeight: '700',
										}}
									>
										Auctioning {pixelad?.name}
									</p>
								</div>
								<div>
									<p className="mb-2">What is the initial value?</p>
									<div style={{ position: 'relative' }}>
										<input
											value={auctionTime}
											onChange={handleChangeAuctionTime}
											placeholder="for example: 60"
											type="text"
											style={{
												width: '100%',
												background: '#374151',
												border: '1px solid #4B5563',
												borderRadius: '8px',
											}}
										/>
										<p
											style={{
												position: 'absolute',
												top: '50%',
												transform: 'translateY(-50%',
												right: '12px',
											}}
										>
											Min
										</p>
									</div>
								</div>
								<div>
									<p className="mb-2">how long will the auction be?</p>
									<div style={{ position: 'relative' }}>
										<input
											value={initialValue}
											onChange={handleChangeInitialValue}
											placeholder="for example: 0.1"
											type="text"
											style={{
												width: '100%',
												background: '#374151',
												border: '1px solid #4B5563',
												borderRadius: '8px',
											}}
										/>
										<p
											style={{
												position: 'absolute',
												top: '50%',
												transform: 'translateY(-50%',
												right: '12px',
											}}
										>
											ETH
										</p>
									</div>
								</div>
								<div>
									<p className="mb-1">Auction URL</p>
									<div className="flex gap-3">
										<p>https://www.google&oq...</p>
										<img src={icons.copy} alt="" />
									</div>
								</div>
							</div>
						),
						calling: (
							<>
								<div className="spinner">
									<Spinner size={'xl'} />
								</div>
								<h2 className="home-modal_heading">This might take a few moments</h2>
								<p className="home-modal_content">
									You can close this window and we will let you know when your pixelad is ready.
								</p>

								<Button
									disabled
									style={{
										width: '375px',
									}}
								>
									Generating your pixelad...
								</Button>
							</>
						),

						done: (
							<>
								<h2 className="home-modal_heading">Your pixelad is ready! </h2>

								<div className="new-pixelad_card-wrapper"></div>

								<Link to="/gallery">
									<Button
										style={{
											width: '375px',
										}}
									>
										View in gallery
									</Button>
								</Link>
							</>
						),

						delay: (
							<>
								<h2 className="home-modal_heading">It seems like it's taking too long...</h2>
								<p className="home-modal_content">
									Your Pixelad is on it's way and will be in your gallery shortly
								</p>
								<Link to="/gallery">
									<Button
										style={{
											width: '375px',
										}}
									>
										View in gallery
									</Button>
								</Link>
							</>
						),

						error: (
							<>
								<img className="ml-4" src={fail} alt="Fail image." />
								<div
									style={{
										width: '375px',
									}}
								>
									<h2 className="text-center mb-3 text-2xl font-bold">Pixelad was not generated</h2>
									<p className="text-center text-lg">
										We had a problem generating your Pixelad. Please try again.
									</p>
								</div>
								<Button onClick={() => setGenerateStatus('initial')}>Try again</Button>
							</>
						),
					}[status]
				}
			</ModalBody>

			{['initial', 'settingData'].includes(status) && (
				<Modal.Footer
					style={{
						display: 'flex',
						justifyContent: 'space-between',
					}}
					className="bg-gray-700"
				>
					<div className="flex items-center gap-2">
						<button className="cancel" onClick={handleCloseModal}>
							Cancel
						</button>
					</div>
					{status === 'initial' && (
						<Button
							style={{
								borderRadius: '6px',
							}}
							type="submit"
							disabled={!pixelad}
							onClick={() => setGenerateStatus('settingData')}
						>
							Next
						</Button>
					)}
					{status === 'settingData' && (
						<Button
							style={{
								borderRadius: '6px',
							}}
							type="submit"
							onClick={handleStartAuction}
						>
							Start auction
						</Button>
					)}
				</Modal.Footer>
			)}
		</Modal>
	);
};

export default BidNFT;
